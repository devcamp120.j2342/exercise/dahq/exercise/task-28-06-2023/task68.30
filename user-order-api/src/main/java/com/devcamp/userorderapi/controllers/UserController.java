package com.devcamp.userorderapi.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userorderapi.models.User;
import com.devcamp.userorderapi.repositorys.UserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @PostMapping("/user/create")
    public ResponseEntity<User> createUser(@RequestBody User pUser) {
        try {
            User cUser = new User();
            cUser.setCreated_at(new Date());
            cUser.setUpdated_at(null);
            cUser.setFullName(pUser.getFullName());
            cUser.setEmail(pUser.getEmail());
            cUser.setPhone(pUser.getPhone());
            cUser.setAddress(pUser.getAddress());
            cUser.setOrders(pUser.getOrders());

            User vUser = userRepository.save(cUser);

            return new ResponseEntity<>(vUser, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/all")
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @PutMapping("/user/update/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User pUser, @PathVariable("id") long id) {
        Optional<User> vUser = userRepository.findById(id);
        if (vUser.isPresent()) {

            vUser.get().setFullName(pUser.getFullName());
            vUser.get().setEmail(pUser.getEmail());
            vUser.get().setPhone(pUser.getPhone());
            vUser.get().setAddress(pUser.getAddress());
            vUser.get().setOrders(pUser.getOrders());
            return new ResponseEntity<User>(userRepository.save(vUser.get()), HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/user/details/{id}")
    public User getUserById(@PathVariable("id") long id) {

        return userRepository.findById(id).get();
    }
}
