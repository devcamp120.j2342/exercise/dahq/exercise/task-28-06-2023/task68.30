package com.devcamp.userorderapi.repositorys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorderapi.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByUserId(long id);

}
