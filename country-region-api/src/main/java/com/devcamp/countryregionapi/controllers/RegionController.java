package com.devcamp.countryregionapi.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregionapi.models.Country;
import com.devcamp.countryregionapi.models.Region;
import com.devcamp.countryregionapi.reponsitorys.CountryRepository;
import com.devcamp.countryregionapi.reponsitorys.RegionRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class RegionController {
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    CountryRepository countryRepository;

    @PostMapping("/region/create/{id}")
    public ResponseEntity<Region> createRegion(@RequestBody Region pRegion, @PathVariable("id") long id) {
        try {
            Optional<Country> cCountry = countryRepository.findById(id);
            Region cRegion = new Region();
            cRegion.setRegionCode(pRegion.getRegionCode());
            cRegion.setRegionName(pRegion.getRegionName());
            cRegion.setCountry(cCountry.get());

            return new ResponseEntity<>(regionRepository.save(cRegion), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/all")
    public List<Region> getAllRegion() {
        return regionRepository.findAll();
    }

    @PutMapping("/region/update/{id}")
    public ResponseEntity<Region> updateRegion(@RequestBody Region pRegion, @PathVariable("id") long id) {
        try {
            Optional<Region> cRegion = regionRepository.findById(id);
            cRegion.get().setRegionCode(pRegion.getRegionCode());
            cRegion.get().setRegionName(pRegion.getRegionName());
            return new ResponseEntity<>(regionRepository.save(cRegion.get()), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @DeleteMapping("region/delete/{id}")
    public ResponseEntity<Region> deleteRegion(@PathVariable("id") long id) {
        regionRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @GetMapping("region/details/{id}")
    public Region getRegionById(@PathVariable("id") long id) {
        return regionRepository.findById(id).get();
    }

    @GetMapping("/region/countryId/{id}")
    public List<Region> getRegionByCountryId(@PathVariable("id") long id) {
        Optional<Country> vCountry = countryRepository.findById(id);
        return vCountry.get().getRegions();
    }

}
